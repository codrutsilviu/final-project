package pageObjects;

import helperMethods.FluentWait;
import helperMethods.HelperList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;

public class ProductPage {

    private WebDriver driver;
    private HelperList helperList = new HelperList();
    private By secondaryContainerTitle = By.xpath("//*[@class='title']");
    private By menuBurgerBtn = By.id("react-burger-menu-btn");
    private By shoppingCartLink = By.id("shopping_cart_container");
    private By filterDropdown = By.xpath("//*[@data-test='product_sort_container']");
    private By inventoryContainer = By.id("inventory_container");
    private By filterDropdownOptions = By.xpath("//*[@data-test='product_sort_container']/option");
    private By filterSelectedOption = By.xpath("//*[@class='active_option']");
    private By itemTitles = By.xpath("//*[@class='inventory_item_name']");
    private By itemDescription = By.xpath("//*[@class='inventory_item_desc']");
    private By itemPrices = By.xpath("//*[@class='inventory_item_price']");
    private By itemAddToCartButton = By.xpath("//*[@class='btn btn_primary btn_small btn_inventory']");
    private By itemRemoveFromCartButton = By.xpath("//*[@class='btn btn_secondary btn_small btn_inventory']");
    private By twitterLink = By.xpath("//*[@class='social_twitter']");
    private By facebookLink = By.xpath("//*[@class='social_facebook']");
    private By linkedinLink = By.xpath("//*[@class='social_linkedin']");
    private By footerText = By.xpath("//*[@class='footer_copy']");
    private By shoppingCartBadgeNumber = By.xpath("//*[@class='shopping_cart_badge']");

    public ProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getCurrentURL() {
        return driver.getCurrentUrl();
    }

    public String getTitle() {
        return driver.findElement(secondaryContainerTitle).getText();
    }

    public boolean menuButtonIsDisplayed() {
        return driver.findElement(menuBurgerBtn).isDisplayed();
    }

    public boolean shoppingCartIsDisplayed() {
        return driver.findElement(shoppingCartLink).isDisplayed();
    }

    public boolean filterDropdownIsDisplayed() {
        return driver.findElement(filterDropdown).isDisplayed();
    }

    public boolean inventoryListIsDisplayed() {
        return driver.findElement(inventoryContainer).isDisplayed();
    }

    public boolean twitterLinkIsDisplayed() {
        return driver.findElement(twitterLink).isDisplayed();
    }

    public boolean facebookLinkIsDisplayed() {
        return driver.findElement(facebookLink).isDisplayed();
    }

    public boolean linkedinLinkIsDisplayed() {
        return driver.findElement(linkedinLink).isDisplayed();
    }

    public String getFooterText() {
        return driver.findElement(footerText).getText();
    }

    public String getProductTitleAtPosition(int position) {
        List<WebElement> elements = driver.findElements(itemTitles);
        return helperList.getTextAtPosition(elements, position);
    }

    public String getProductDescriptionAtPosition(int position) {
        List<WebElement> elements = driver.findElements(itemDescription);
        return helperList.getTextAtPosition(elements, position);
    }

    public String getProductPriceAtPosition(int position) {
        List<WebElement> elements = driver.findElements(itemPrices);
        return helperList.getTextAtPosition(elements, position);
    }

    public String getProductAddToCartBtnAtPosition(int position) {
        List<WebElement> elements = driver.findElements(itemAddToCartButton);
        return helperList.getTextAtPosition(elements, position);
    }

    public String getProductRemoveBtnText() {
        return driver.findElement(itemRemoveFromCartButton).getText();
    }

    public void selectTextInDropdown(String text) {
        driver.findElement(filterDropdown).click();

        List<WebElement> options = driver.findElements(filterDropdownOptions);
        for (WebElement option : options) {
            if (option.getText().equals(text)) {
                option.click();
                return;
            }
        }
    }

    public String getFilterSelectedOption() {
        return driver.findElement(filterSelectedOption).getText();
    }

    public boolean itemsTitleAreOrderedDescending() {
        List<WebElement> elements = driver.findElements(itemTitles);
        return helperList.checkTextDescendingOrder(elements);
    }

    public boolean itemsTitleAreOrderedAsscending() {
        List<WebElement> elements = driver.findElements(itemTitles);
        return helperList.checkTextAscendingOrder(elements);
    }

    public boolean pricesAreOrderedAscending() {
        List<WebElement> elements = driver.findElements(itemPrices);
        return helperList.checkPricesAscendingOrder(elements);
    }

    public boolean pricesAreOrderedDesscending() {
        List<WebElement> elements = driver.findElements(itemPrices);
        return helperList.checkPricesDescendingOrder(elements);
    }

    public void pressAddToCartBtn() {
        driver.findElement(itemAddToCartButton).click();
    }

    public int getShoppingCartBadgeNumber() {
        return Integer.parseInt(driver.findElement(shoppingCartBadgeNumber).getText());
    }

    public void pressRemoveBtn() {
        driver.findElement(itemRemoveFromCartButton).click();
    }

    public boolean shoppingCartBadgeIsDisplayed() {
        boolean isDisplayed = false;
        try {
            isDisplayed = driver.findElement(shoppingCartBadgeNumber).isDisplayed();
        } catch (NoSuchElementException e) {
        }
        return isDisplayed;
    }

    public void pressShoppingCartBtn() {
        driver.findElement(shoppingCartLink).click();
    }

    public void pressAddToCartBtnAtPosition(int position) {
        List<WebElement> addToCartButtons = driver.findElements(itemAddToCartButton);
        addToCartButtons.get(position).click();
    }

    public void waitForPageUrl() {
        new FluentWait(driver, secondaryContainerTitle);
    }
}
