package pageObjects;

import helperMethods.FluentWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;
    private By usernameInput = By.id("user-name");
    private By passwordInput = By.id("password");
    private By loginBtn = By.id("login-button");
    private By error = By.xpath("//*[@data-test='error']");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }



    public void openLoginPage() {
        driver.get("https://www.saucedemo.com/");
    }

    public void fillUsername(String str) {
        driver.findElement(usernameInput).sendKeys(str);
    }

    public void fillPassword(String str) {
        driver.findElement(passwordInput).sendKeys(str);
    }

    public void pressLoginButton() {
        driver.findElement(loginBtn).click();
    }

    public String getErrorText() {
        return driver.findElement(error).getText();
    }

    public void loginSuccessfully(String username, String password) {
        driver.get("https://www.saucedemo.com/");
        driver.findElement(usernameInput).sendKeys(username);
        driver.findElement(passwordInput).sendKeys(password);
        driver.findElement(loginBtn).click();
    }

    public String getLoginBtnBackgroundColor() {
        return driver.findElement(loginBtn).getCssValue("background-color");
    }

    public String getLoginBtnBorderColor() {
        return driver.findElement(loginBtn).getCssValue("border");
    }

    public String getLoginBtnColor() {
        return driver.findElement(loginBtn).getCssValue("color");
    }

    public boolean loginBtnIsEnabled() {
        return driver.findElement(loginBtn).isEnabled();
    }

    public void waitAndPressLoginButton() {
        new FluentWait(driver, loginBtn).waitForElement();
        driver.findElement(loginBtn).click();
    }
}
