package pageObjects;

import helperMethods.HelperList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShoppingCartPage {

    private WebDriver driver;
    private By secondaryContainerTitle = By.xpath("//*[@class='title']");
    private By shoppingCart = By.id("shopping_cart_container");
    private By shoppingCartBadge = By.xpath("//*[@class='shopping_cart_badge']");
    private By cartQuantityLabel = By.xpath("//*[@class='cart_quantity_label']");
    private By cartDescriptionLabel = By.xpath("//*[@class='cart_desc_label']");
    private By cartItem = By.xpath("//*[@class='cart_item']");
    private By cartQuantity = By.xpath("//*[@class='cart_quantity']");
    private By cartItemName = By.xpath("//*[@class='inventory_item_name']");
    private By cartItemDescription = By.xpath("//*[@class='inventory_item_desc']");
    private By cartItemPrice = By.xpath("//*[@class='inventory_item_price']");
    private By cartRemoveBtn = By.xpath("//*[@class='btn btn_secondary btn_small cart_button']");
    private By continueShoppingBtn = By.id("continue-shopping");
    private By checkoutBtn = By.id("checkout");
    public HelperList helperList = new HelperList();

    public ShoppingCartPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageUrl() {
       return driver.getCurrentUrl();
    }

    public String getPageTitle() {
        return driver.findElement(secondaryContainerTitle).getText();
    }

    public String getQuantityLabel() {
        return driver.findElement(cartQuantityLabel).getText();
    }

    public String getDescriptionLabel() {
        return driver.findElement(cartDescriptionLabel).getText();
    }

    public boolean isCartContainerDisplayed() {
        return driver.findElement(shoppingCart).isDisplayed();
    }

    public String getContinueShoppingBtnText() {
        return driver.findElement(continueShoppingBtn).getText();
    }

    public String getCheckoutBtnText() {
        return driver.findElement(checkoutBtn).getText();
    }

    public void pressContinueShoppingButton() {
        driver.findElement(continueShoppingBtn).click();
    }

    public String getProductTitleAtPosition(int position) {
        List<WebElement> titleTexts = driver.findElements(cartItemName);
        return helperList.getTextAtPosition(titleTexts, position);
    }

    public String getProductDescriptionAtPosition(int position) {
        List<WebElement> descriptionTexts = driver.findElements(cartItemDescription);
        return helperList.getTextAtPosition(descriptionTexts, position);
    }

    public String getProductPriceAtPosition(int position) {
        List<WebElement> priceTexts = driver.findElements(cartItemPrice);
        return helperList.getTextAtPosition(priceTexts, position);
    }

    public void pressRemoveBtnAtPosition(int position) {
        List<WebElement> removeBtn = driver.findElements(cartRemoveBtn);
        removeBtn.get(position).click();
    }

    public void pressCheckoutBtn() {
        driver.findElement(checkoutBtn).click();
    }
}
