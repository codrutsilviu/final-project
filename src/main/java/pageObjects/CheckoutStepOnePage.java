package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutStepOnePage {

    private WebDriver driver;
    private By secondaryContainerTitle = By.xpath("//*[@class='title']");
    private By firstNameField = By.id("first-name");
    private By lastNameField = By.id("last-name");
    private By postalCodeField = By.id("postal-code");
    private By cancelBtn = By.id("cancel");
    private By continueBtn = By.name("continue");
    private By fieldError = By.xpath("//*[@data-test='error']");

    public CheckoutStepOnePage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageUrl() {
        return driver.getCurrentUrl();
    }

    public String getPageTextTitle() {
        return driver.findElement(secondaryContainerTitle).getText();
    }

    public String getFirstNameFieldPlaceholder() {
        return driver.findElement(firstNameField).getAttribute("placeholder");
    }

    public String getLastNameFieldPlaceholder() {
        return driver.findElement(lastNameField).getAttribute("placeholder");
    }

    public String getPostalCodeFieldPlaceholder() {
        return driver.findElement(postalCodeField).getAttribute("placeholder");
    }

    public String getCancelBtnText() {
        return driver.findElement(cancelBtn).getText();
    }

    public String getContinueBtnText() {
        return driver.findElement(continueBtn).getAttribute("value");
    }

    public void fillLastNameField(String lastname) {
        driver.findElement(lastNameField).click();
        driver.findElement(lastNameField).sendKeys(lastname);
    }

    public void fillPostalCodeField(int postalCode) {
        driver.findElement(postalCodeField).click();
        driver.findElement(postalCodeField).sendKeys(String.valueOf(postalCode));
    }

    public void pressContinueBtn() {
        driver.findElement(continueBtn).click();
    }

    public String getFieldError() {
        return driver.findElement(fieldError).getText();
    }

    public void fillFirstNameField(String firstName) {
        driver.findElement(firstNameField).click();
        driver.findElement(firstNameField).sendKeys(firstName);
    }
}
