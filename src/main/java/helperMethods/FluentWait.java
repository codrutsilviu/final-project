package helperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;
import java.time.Duration;
import java.util.function.Function;

public class FluentWait {

    private WebDriver driver;
    private By locator;

    public FluentWait(WebDriver driver, By locator) {
        this.driver = driver;
        this.locator = locator;
    };

    public void waitForElement() {
        Wait<WebDriver> wait = new org.openqa.selenium.support.ui.FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class);

        wait.until(new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                System.out.println("Wait performed");
                WebElement element = webDriver.findElement(locator);
                return element.isDisplayed();
            }
        });
    }
}
