package helperMethods;

import com.google.common.collect.Ordering;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class HelperList {

    public boolean checkTextDescendingOrder(List<WebElement> elements) {
        List<String> titles = new ArrayList<>();

        for (WebElement element: elements) {
            titles.add(element.getText());
        }

        return Ordering.<String> natural().reverse().isOrdered(titles);
    }

    public boolean checkTextAscendingOrder(List<WebElement> elements) {
        List<String> titles = new ArrayList<>();

        for (WebElement element: elements) {
            titles.add(element.getText());
        }

        return Ordering.<String> natural().isOrdered(titles);
    }

    public boolean checkPricesAscendingOrder(List<WebElement> elements) {
        List<Double> doubles = new ArrayList<>();
        for (WebElement element: elements) {
            String priceNumber = element.getText().substring(1);
            doubles.add(Double.valueOf(priceNumber));
        }
        return Ordering.<Double> natural().isOrdered(doubles);
    }

    public boolean checkPricesDescendingOrder(List<WebElement> elements) {
        List<Double> doubles = new ArrayList<>();
        for (WebElement element: elements) {
            String priceNumber = element.getText().substring(1);
            doubles.add(Double.valueOf(priceNumber));
        }
        return Ordering.<Double> natural().reverse().isOrdered(doubles);
    }

    public String getTextAtPosition(List<WebElement> elements, int position) {
        return elements.get(position).getText();
    }
}
