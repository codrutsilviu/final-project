Feature: Test Product page

  Background: Login
    When Login with user "standard_user" and password "secret_sauce"
    Then Check Product page URL

  Scenario: Check page elements
    Then Menu button is displayed
    And Check Product page title
    And Shopping cart is displayed
    And Filter dropdown is displayed
    And Inventory list is displayed
    And Twitter link is displayed
    And Facebook link is displayed
    And Linkedin link is displayed
    And Check footer page text

  Scenario: Check first product
    Then Check product title at position 0
    And Check product description at position 0
    And Check product price at position 0
    And Check product Add to cart button name at position 0

  Scenario: Check default filter functionality "Name (A to Z)"
    When Press expand Filter dropdown and select "Name (A to Z)"
    Then Filter value is "NAME (A TO Z)"
    And Inventory items are ordered ascending

  Scenario: Check filter functionality "Name (Z to A)"
    When Press expand Filter dropdown and select "Name (Z to A)"
    Then Filter value is "NAME (Z TO A)"
    And Inventory items are ordered descending

  Scenario: Check default filter functionality "Price (low to high)"
    When Press expand Filter dropdown and select "Price (low to high)"
    Then Filter value is "PRICE (LOW TO HIGH)"
    And Price items are ordered ascending

  Scenario: Check filter functionality "Price (high to low)"
    When Press expand Filter dropdown and select "Price (high to low)"
    Then Filter value is "PRICE (HIGH TO LOW)"
    And Price items are ordered descending

  Scenario: Check "ADD TO CART" button functionality
    When Press ADD TO CART button
    Then Check REMOVE button text
    And Check shopping cart badge is 1

  Scenario: Check "Remove" button functionality
    When Press ADD TO CART button
    And Press REMOVE button
    Then Check shopping cart badge is not displayed