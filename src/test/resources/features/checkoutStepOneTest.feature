Feature: Test Checkout: Step One page

  Background: Open Checkout: Step One page
    When Login with user "performance_glitch_user" and password "secret_sauce"
    And Press ADD TO CART button at position 0
    And Press ADD TO CART button at position 1
    And Press Shopping Cart button
    And Press  Checkout button

  Scenario: Check page elements
    Given Checkout: Step One page
    Then Check Checkout: Step One page title
    And Check First Name field placeholder
    And Check Second Name field placeholder
    And Check Postal Code field placeholder
    And Check Cancel button text
    And Check Continue button 'value' attribute

  Scenario: Check First Name field error
    Given Checkout: Step One page
    When Fill Last Name field with "last name"
    And Fill Postal Code field with 077160
    And Press Continue button
    Then Check First Name field error

  Scenario: Check Last Name field error
    Given Checkout: Step One page
    When Fill First Name field with "first name"
    And Fill Postal Code field with 077160
    And Press Continue button
    Then Check Last Name field error

  Scenario: Check Postal Code field error
    Given Checkout: Step One page
    When Fill First Name field with "first name"
    And Fill Last Name field with "last name"
    And Press Continue button
    Then Check Postal Code field error