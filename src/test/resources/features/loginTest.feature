Feature: Test Login Page

  Scenario: Login with standard user
    When Login with user "standard_user" and password "secret_sauce"
    Then Check Product page URL

  Scenario: Login with locked out user
    Given Login Page is displayed
    When Fill username field with "locked_out_user"
    And Fill password field with "secret_sauce"
    And Press Login button
    Then Display locked out user error

  Scenario: Login with problem user
    Given Login Page is displayed
    When Fill username field with "problem_user"
    And Fill password field with "secret_sauce"
    And Press Login button
    Then Check Product page URL

  Scenario: Login with performance glitch user
    Given Login Page is displayed
    When Fill username field with "performance_glitch_user"
    And Fill password field with "secret_sauce"
    And Wait and press Login button
    Then Wait and check Product page URL

  Scenario: Login without username
    Given Login Page is displayed
    When Fill password field with "secret_sauce"
    And Press Login button
    Then Display username required error

  Scenario: Login without password
    Given Login Page is displayed
    When Fill username field with "performance_glitch_user"
    And Press Login button
    Then Display password required error

  Scenario: Login with wrong username and password pair
    Given Login Page is displayed
    When Fill username field with "standard_user"
    And Fill password field with "12345678"
    And Press Login button
    Then Display username and password do not match error

  Scenario: Check Login button colors
    Given Login Page is displayed
    Then Login button is enabled
    And Login button background color is "rgba(226, 35, 26, 1)"
    And Login button border color is "1.99528px solid rgb(226, 35, 26)"
    And Login button color is "rgba(255, 255, 255, 1)"