Feature: Test Shopping Cart

  Background: Login
    When Login with user "performance_glitch_user" and password "secret_sauce"
    Then Check Product page URL

  Scenario: Check page elements with empty shopping cart
    When Press Shopping Cart button
    Then Check Shopping Cart page URL
    And Shopping Cart page title is displayed
    And Shopping Cart container is displayed
    And Check shopping cart badge is not displayed
    And Quantity label is displayed
    And Description label is displayed
    And Continue Shopping button is displayed
    And Checkout button is displayed

  Scenario: "CONTINUE SHOPPING" button functionality
    When Press Shopping Cart button
    Then Check Shopping Cart page URL
    When Press Continue Shopping button
    Then Check Product page URL

  Scenario: Check products added to shopping cart
    When Press ADD TO CART button at position 0
    And Press ADD TO CART button at position 1
    And Press Shopping Cart button
    Then Check first product title at position 0
    And Check first product description at position 0
    And Check first product price at position 0
    And Check second product title at position 1
    And Check second product description at position 1
    And Check second product price at position 1

  Scenario: Remove product from shopping cart
    When Press ADD TO CART button at position 0
    And Press ADD TO CART button at position 1
    And Press Shopping Cart button
    Then Check first product title at position 0
    And Check second product title at position 1
    When Press Remove button at position 0
    Then Check first product title at position 0 not displayed
    And Check second product title at position 0