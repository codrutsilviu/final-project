package stepDefinisions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageObjects.LoginPage;
import utils.Constants;

public class LoginSteps {

    LoginPage loginPage = new LoginPage(Hooks.driver);

    @Given("Login Page is displayed")
    public void loginPageIsDisplayed() {
        loginPage.openLoginPage();
    }

    @When("Fill username field with {string}")
    public void fillUsernameField(String str) {
        loginPage.fillUsername(str);
    }

    @And("Fill password field with {string}")
    public void fillPasswordFieldWith(String str) {
        loginPage.fillPassword(str);
    }

    @And("Press Login button")
    public void pressLoginButton() {

        loginPage.pressLoginButton();
    }

    @Then("Display locked out user error")
    public void displayLockedOutUserError() {
        Assert.assertEquals(Constants.lockedOutUserError, loginPage.getErrorText());
    }

    @Then("Display username required error")
    public void displayUsernameRequiredError() {
        Assert.assertEquals(Constants.usernameRequiredError, loginPage.getErrorText());
    }

    @Then("Display password required error")
    public void displayPasswordRequiredError() {
        Assert.assertEquals(Constants.passwordRequiredError, loginPage.getErrorText());
    }

    @Then("Display username and password do not match error")
    public void displayUsernameAndPasswordDoNotMatchError() {
        Assert.assertEquals(Constants.usernameAndPasswordDoNotMatchError, loginPage.getErrorText());
    }

    @When("Login with user {string} and password {string}")
    public void loginWithUserAndPassword(String username, String password) {
        loginPage.loginSuccessfully(username, password);
    }

    @Then("Login button is enabled")
    public void loginButtonIsEnabled() {
        Assert.assertTrue(loginPage.loginBtnIsEnabled());
    }

    @And("Login button background color is {string}")
    public void loginButtonBackgroundColorIs(String color) {
        Assert.assertEquals(color, loginPage.getLoginBtnBackgroundColor());
    }

    @And("Login button border color is {string}")
    public void loginButtonBorderColorIs(String color) {
        Assert.assertEquals(color, loginPage.getLoginBtnBorderColor());
    }

    @And("Login button color is {string}")
    public void loginButtonColorIs(String color) {
        Assert.assertEquals(color, loginPage.getLoginBtnColor());
    }

    @And("Wait and press Login button")
    public void waitAndPressLoginButton() {
        loginPage.waitAndPressLoginButton();
    }
}
