package stepDefinisions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageObjects.CheckoutStepOnePage;
import utils.Constants;

public class CheckoutStepOneSteps {

    CheckoutStepOnePage checkoutStepOnePage = new CheckoutStepOnePage(Hooks.driver);

    @Then("Checkout: Step One page")
    public void checkoutStepOnePageIsDisplayed() {
        Assert.assertEquals(Constants.checkoutSteoOneURL, checkoutStepOnePage.getPageUrl());
    }

    @Then("Check Checkout: Step One page title")
    public void checkCheckoutStepOnePageTitle() {
        Assert.assertEquals(Constants.checkoutSteoOnePageTitle, checkoutStepOnePage.getPageTextTitle());
    }

    @And("Check First Name field placeholder")
    public void checkFirstNameFieldPlaceholder() {
        Assert.assertEquals(Constants.firstNameFieldPlaceholder, checkoutStepOnePage.getFirstNameFieldPlaceholder());
    }

    @And("Check Second Name field placeholder")
    public void checkSecondNameFieldPlaceholder() {
        Assert.assertEquals(Constants.lastNameFieldPlaceholder, checkoutStepOnePage.getLastNameFieldPlaceholder());
    }

    @And("Check Postal Code field placeholder")
    public void checkPostalCodeFieldPlaceholder() {
        Assert.assertEquals(Constants.postalCodeFieldPlaceholder, checkoutStepOnePage.getPostalCodeFieldPlaceholder());
    }

    @And("Check Cancel button text")
    public void checkCancelButtonIsDisplayed() {
        Assert.assertEquals(Constants.cancelBtnText, checkoutStepOnePage.getCancelBtnText());
    }

    @And("Check Continue button 'value' attribute")
    public void checkContinueButtonIsDisplayed() {
        Assert.assertEquals(Constants.continueBtnText, checkoutStepOnePage.getContinueBtnText());
    }

    @When("Fill Last Name field with {string}")
    public void fillLastNameFieldWith(String lastName) {
        checkoutStepOnePage.fillLastNameField(lastName);
    }

    @And("Fill Postal Code field with {int}")
    public void fillPostalCodeFieldWith(int postalCode) {
        checkoutStepOnePage.fillPostalCodeField(postalCode);
    }

    @And("Press Continue button")
    public void pressContinueButton() {
        checkoutStepOnePage.pressContinueBtn();
    }

    @Then("Check First Name field error")
    public void checkFirstNameFieldError() {
        Assert.assertEquals(Constants.firstNameFieldError, checkoutStepOnePage.getFieldError());
    }

    @When("Fill First Name field with {string}")
    public void fillFirstNameFieldWith(String firstName) {
        checkoutStepOnePage.fillFirstNameField(firstName);
    }

    @Then("Check Last Name field error")
    public void checkLastNameFieldError() {
        Assert.assertEquals(Constants.lastNameFieldError, checkoutStepOnePage.getFieldError());
    }

    @Then("Check Postal Code field error")
    public void checkPostalCodeFieldError() {
        Assert.assertEquals(Constants.postalCodeFieldError, checkoutStepOnePage.getFieldError());
    }
}
