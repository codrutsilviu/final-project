package stepDefinisions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageObjects.ShoppingCartPage;
import utils.Constants;

public class ShppingCartSteps {

    ShoppingCartPage shoppingCartPage = new ShoppingCartPage(Hooks.driver);

    @Then("Check Shopping Cart page URL")
    public void checkShoppingCartPageURL() {
        Assert.assertEquals(Constants.shoppingCartPageURL, shoppingCartPage.getPageUrl());
    }

    @And("Shopping Cart page title is displayed")
    public void shoppingCartPageTitleIsDisplayed() {
        Assert.assertEquals(Constants.shoppingCartPageTitle, shoppingCartPage.getPageTitle());
    }

    @And("Shopping Cart container is displayed")
    public void shoppingCartContainerIsDisplayed() {
        Assert.assertTrue(shoppingCartPage.isCartContainerDisplayed());
    }

    @And("Quantity label is displayed")
    public void quantityLabelIsDisplayed() {
        Assert.assertEquals(Constants.shoppingCartQuantityLabel, shoppingCartPage.getQuantityLabel());
    }

    @And("Description label is displayed")
    public void descriptionLabelIsDisplayed() {
        Assert.assertEquals(Constants.shoppingCartItemDescriptionLabel, shoppingCartPage.getDescriptionLabel());
    }

    @And("Continue Shopping button is displayed")
    public void continueShoppingButtonIsDisplayed() {
        Assert.assertEquals(Constants.continueShoppingBtn, shoppingCartPage.getContinueShoppingBtnText());
    }

    @And("Checkout button is displayed")
    public void checkoutButtonIsDisplayed() {
        Assert.assertEquals(Constants.checkoutBtn, shoppingCartPage.getCheckoutBtnText());
    }

    @When("Press Continue Shopping button")
    public void pressContinueShoppingButton() {
        shoppingCartPage.pressContinueShoppingButton();
    }

    @Then("Check first product title at position {int}")
    public void checkFirstProductTitleAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductTitle, shoppingCartPage.getProductTitleAtPosition(position));
    }

    @And("Check first product description at position {int}")
    public void checkFirstProductDescriptionAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductDescription, shoppingCartPage.getProductDescriptionAtPosition(position));
    }

    @And("Check first product price at position {int}")
    public void checkFirstProductPriceAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductPrice, shoppingCartPage.getProductPriceAtPosition(position));
    }

    @And("Check second product title at position {int}")
    public void checkSecondProductTitleAtPosition(int position) {
        Assert.assertEquals(Constants.secondProductTitle, shoppingCartPage.getProductTitleAtPosition(position));
    }

    @And("Check second product description at position {int}")
    public void checkSecondProductDescriptionAtPosition(int position) {
        Assert.assertEquals(Constants.secondProductDescription, shoppingCartPage.getProductDescriptionAtPosition(position));
    }

    @And("Check second product price at position {int}")
    public void checkSecondProductPriceAtPosition(int position) {
        Assert.assertEquals(Constants.secondProductPrice, shoppingCartPage.getProductPriceAtPosition(position));
    }

    @When("Press Remove button at position {int}")
    public void pressRemoveButtonAtPosition(int position) {
        shoppingCartPage.pressRemoveBtnAtPosition(position);
    }

    @Then("Check first product title at position {int} not displayed")
    public void checkFirstProductTitleAtPositionNotDisplayed(int position) {
        Assert.assertNotEquals(Constants.firstProductTitle, shoppingCartPage.getProductTitleAtPosition(position));
    }

    @And("Press  Checkout button")
    public void pressCheckoutButton() {
        shoppingCartPage.pressCheckoutBtn();
    }
}
