package stepDefinisions;

import helperMethods.FluentWait;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageObjects.ProductPage;
import utils.Constants;

public class ProductSteps {

    ProductPage productPage = new ProductPage(Hooks.driver);

    @Then("Check Product page URL")
    public void checkProductPageURL() {
        Assert.assertEquals(Constants.productsPageURL, productPage.getCurrentURL());
    }

    @Then("Check Product page title")
    public void checkProductPageTitle() {
        Assert.assertEquals(Constants.productsPageTitle, productPage.getTitle());
    }

    @Then("Menu button is displayed")
    public void menuButtonIsDisplayed() {
        Assert.assertTrue(productPage.menuButtonIsDisplayed());
    }

    @And("Shopping cart is displayed")
    public void shoppingCartIsDisplayed() {
        Assert.assertTrue(productPage.shoppingCartIsDisplayed());
    }

    @And("Filter dropdown is displayed")
    public void filterButtonIsDisplayed() {
        Assert.assertTrue(productPage.filterDropdownIsDisplayed());
    }

    @And("Inventory list is displayed")
    public void inventoryListIsDisplayed() {
        Assert.assertTrue(productPage.inventoryListIsDisplayed());
    }

    @And("Twitter link is displayed")
    public void twitterLinkIsDisplayed() {
        Assert.assertTrue(productPage.twitterLinkIsDisplayed());
    }

    @And("Facebook link is displayed")
    public void facebookLinkIsDisplayed() {
        Assert.assertTrue(productPage.facebookLinkIsDisplayed());
    }

    @And("Linkedin link is displayed")
    public void linkedinLinkIsDisplayed() {
        Assert.assertTrue(productPage.linkedinLinkIsDisplayed());
    }

    @And("Check footer page text")
    public void checkFooterPageText() {
        Assert.assertTrue(productPage.getFooterText().contains(Constants.productFooterText));
    }

    @Then("Check product title at position {int}")
    public void checkProductTitleAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductTitle, productPage.getProductTitleAtPosition(position));
    }

    @And("Check product description at position {int}")
    public void checkProductDescriptionAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductDescription, productPage.getProductDescriptionAtPosition(position));
    }

    @And("Check product price at position {int}")
    public void checkProductPriceAtPosition(int position) {
        Assert.assertEquals(Constants.firstProductPrice, productPage.getProductPriceAtPosition(position));
    }

    @And("Check product Add to cart button name at position {int}")
    public void checkProductAddToCartButtonNameAtPosition(int position) {
        Assert.assertEquals(Constants.itemBtnAddToCart, productPage.getProductAddToCartBtnAtPosition(position));
    }

    @When("Press expand Filter dropdown and select {string}")
    public void pressExpandFilterDropdownAndSelect(String text) {
        productPage.selectTextInDropdown(text);
    }

    @Then("Filter value is {string}")
    public void filterValueIs(String text) {
        Assert.assertEquals(text, productPage.getFilterSelectedOption());
    }

    @And("Inventory items are ordered descending")
    public void inventoryItemsAreOrderedDescending() {
        Assert.assertTrue(productPage.itemsTitleAreOrderedDescending());
    }

    @And("Inventory items are ordered ascending")
    public void inventoryItemsAreOrderedAsccending() {
        Assert.assertTrue(productPage.itemsTitleAreOrderedAsscending());
    }

    @And("Price items are ordered ascending")
    public void priceItemsAreOrderedAscending() {
        Assert.assertTrue(productPage.pricesAreOrderedAscending());
    }

    @And("Price items are ordered descending")
    public void priceItemsAreOrderedDescending() {
        Assert.assertTrue(productPage.pricesAreOrderedDesscending());
    }

    @When("Press ADD TO CART button")
    public void pressAddToCartButton() {
        productPage.pressAddToCartBtn();
    }

    @Then("Check REMOVE button text")
    public void checkRemoveButtonText() {
        Assert.assertEquals(Constants.itemBtnRemove, productPage.getProductRemoveBtnText());
    }

    @And("Check shopping cart badge is {int}")
    public void checkShoppingCartBadgeIs(int number) {
        Assert.assertEquals(number, productPage.getShoppingCartBadgeNumber());
    }

    @And("Press REMOVE button")
    public void pressRemoveButton() {
        productPage.pressRemoveBtn();
    }

    @Then("Check shopping cart badge is not displayed")
    public void checkShoppingCartBadgeIsNotDisplayed() {
        Assert.assertFalse(productPage.shoppingCartBadgeIsDisplayed());
    }

    @When("Press Shopping Cart button")
    public void pressShoppingCartButton() {
        productPage.pressShoppingCartBtn();
    }

    @When("Press ADD TO CART button at position {int}")
    public void pressADDTOCARTButtonAtPosition(int position) {
        productPage.pressAddToCartBtnAtPosition(position);
    }

    @Then("Wait and check Product page URL")
    public void waitAndCheckProductPageURL() {
        productPage.waitForPageUrl();
        Assert.assertEquals(Constants.productsPageURL, productPage.getCurrentURL());
    }
}
