package utils;

public class Constants {

//    Login page
    public static final String lockedOutUserError = "Epic sadface: Sorry, this user has been locked out.";
    public static final String usernameRequiredError = "Epic sadface: Username is required";
    public static final String passwordRequiredError = "Epic sadface: Password is required";
    public static final String usernameAndPasswordDoNotMatchError = "Epic sadface: Username and password do not match any user in this service";

//    Product page
    public static final String productsPageURL = "https://www.saucedemo.com/inventory.html";
    public static final String productsPageTitle = "PRODUCTS";
    public static final String productFooterText = "© 2022 Sauce Labs. All Rights Reserved. Terms of Service | Privacy Policy";
    public static final String itemBtnAddToCart = "ADD TO CART";
    public static final String itemBtnRemove = "REMOVE";
    //    Cart items
    public static final String firstProductTitle = "Sauce Labs Backpack";
    public static final String firstProductDescription = "carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.";
    public static final String firstProductPrice = "$29.99";
    public static final String secondProductTitle = "Sauce Labs Bolt T-Shirt";
    public static final String secondProductDescription = "Get your testing superhero on with the Sauce Labs bolt T-shirt. From American Apparel, 100% ringspun combed cotton, heather gray with red bolt.";
    public static final String secondProductPrice = "$15.99";

//    Shopping Cart page
    public static final String shoppingCartPageURL = "https://www.saucedemo.com/cart.html";
    public static final String shoppingCartPageTitle = "YOUR CART";
    public static final String shoppingCartQuantityLabel = "QTY";
    public static final String shoppingCartItemDescriptionLabel = "DESCRIPTION";
    public static final String continueShoppingBtn = "CONTINUE SHOPPING";
    public static final String checkoutBtn = "CHECKOUT";

//    CHECKOUT: YOUR INFORMATION page
    public static final String checkoutSteoOneURL = "https://www.saucedemo.com/checkout-step-one.html";
    public static final String checkoutSteoOnePageTitle = "CHECKOUT: YOUR INFORMATION";
    public static final String firstNameFieldPlaceholder = "First Name";
    public static final String lastNameFieldPlaceholder = "Last Name";
    public static final String postalCodeFieldPlaceholder = "Zip/Postal Code";
    public static final String cancelBtnText = "CANCEL";
    public static final String continueBtnText = "Continue";
    public static final String firstNameFieldError = "Error: First Name is required";
    public static final String lastNameFieldError = "Error: Last Name is required";
    public static final String postalCodeFieldError = "Error: Postal Code is required";
}
